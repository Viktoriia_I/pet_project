import React from 'react'
import './Header.css'
import {Navigation} from '../Navigation/Navigation'
import {Logo} from '../Logo/Logo'


export const Header = () => {
  return (
    <div className="header">
      <Logo />
      <Navigation />
    </div>
  )
}