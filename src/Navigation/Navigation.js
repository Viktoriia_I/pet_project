import React from "react";
import "./Navigation.css";

export const Navigation = () => {
  return (
    <nav className="nav">
      <ul className="navList">
        <li><a href="#">Home</a></li>
        <li><a href="#">Articles</a></li>
        <li><a href="#">Projects</a></li>
        <li><a href="#">About us</a></li>
      </ul>
    </nav>
  );
};
